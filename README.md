# README #

Dark side panel (Folders) for Sublime3 with default Monokai theme. 
To install place this file inside the `~/.config/sublime-text-3/Packages/User` folder. 
Changes should apply instantly.